#pragma once

#include <QMainWindow>

class QLabel;
class QLineEdit;
class QPushButton;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget * parent = nullptr);

private:
	void BuildWindow();

	bool DatabaseConnect();
	bool DatabaseIsReady();
	void DatabaseInit();
	void DatabasePopulate();

private slots:
	void OnInput();
	void OnSearchClicked();

private:
	QLineEdit * mInputText;
	QPushButton * mButtonSearch;
	QLabel * mOutputText;
};

void delay(int millisecondsWait);
