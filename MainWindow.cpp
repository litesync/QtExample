#include "MainWindow.h"

#include <QDebug>
#include <QHBoxLayout>
#include <QIntValidator>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>

MainWindow::MainWindow(QWidget * parent)
	: QMainWindow(parent)
{
	// window title
	setWindowTitle("Qt LiteSync Example");

	// -- set fixed size --
	const int WIN_W = 320;
	const int WIN_H = 180;
	setFixedSize(WIN_W, WIN_H);

	// create GUI
	BuildWindow();

	// -- DATABASE INIT --
	if (DatabaseConnect() == false) return;
	while (!DatabaseIsReady()) {
		delay(200);
	}
	DatabaseInit();
	DatabasePopulate();
}

// ===== PRIVATE =====
void MainWindow::BuildWindow()
{
	QWidget * content = new QWidget;
	setCentralWidget(content);

	// main layout
	QVBoxLayout * layoutMain = new QVBoxLayout;
	content->setLayout(layoutMain);

	// -- VERT SPACER --
	layoutMain->addSpacerItem(new QSpacerItem(10, 10, QSizePolicy::Expanding, QSizePolicy::Expanding));

	// -- INPUT ROW --
	QHBoxLayout * layoutRow = new QHBoxLayout;
	layoutMain->addLayout(layoutRow);

	// input field
	mInputText = new QLineEdit;
	mInputText->setPlaceholderText("1 - 10");
	mInputText->setValidator(new QIntValidator(mInputText));

	connect(mInputText, &QLineEdit::textChanged, this, &MainWindow::OnInput);

	layoutRow->addWidget(mInputText);

	// search button
	mButtonSearch = new QPushButton("SEARCH");
	mButtonSearch->setEnabled(false);

	connect(mButtonSearch, &QPushButton::clicked, this, &MainWindow::OnSearchClicked);

	layoutRow->addWidget(mButtonSearch);

	// -- OUTPUT ROW --
	mOutputText = new QLabel("...");
	mOutputText->setAlignment(Qt::AlignCenter);
	layoutMain->addWidget(mOutputText);

	// -- VERT SPACER --
	layoutMain->addSpacerItem(new QSpacerItem(10, 10, QSizePolicy::Expanding, QSizePolicy::Expanding));
}

bool MainWindow::DatabaseConnect()
{
	const QString DRIVER("QSQLITE");

	if(QSqlDatabase::isDriverAvailable(DRIVER))
	{
		QSqlDatabase db = QSqlDatabase::addDatabase(DRIVER);

		db.setDatabaseName("file:test.db?node=primary&bind=tcp://0.0.0.0:1234");

		if(db.open()) return true;

		qWarning() << "MainWindow::DatabaseConnect - ERROR: " << db.lastError().text();
		mOutputText->setText(db.lastError().text());
	} else {
		qWarning() << "MainWindow::DatabaseConnect - ERROR: no driver " << DRIVER << " available";
		mOutputText->setText("no driver " + DRIVER + " available");
	}

	return false;
}

bool MainWindow::DatabaseIsReady()
{
	QSqlQuery query;

	if(!query.exec("PRAGMA sync_status") || !query.first())
		qWarning() << "MainWindow::DatabaseIsReady - ERROR: " << query.lastError().text();

	QString result = query.value(0).toString();
	mOutputText->setText(result);
	return result.contains("\"db_is_ready\": true");

}

void MainWindow::DatabaseInit()
{
	QSqlQuery query("CREATE TABLE colors (id INTEGER PRIMARY KEY, name TEXT)");

	if(!query.isActive())
		qWarning() << "MainWindow::DatabaseInit - ERROR: " << query.lastError().text();

}

void MainWindow::DatabasePopulate()
{
	QSqlQuery query;
	query.prepare("INSERT INTO colors (name) VALUES(:name)");

	QStringList names = { "Yellow", "Blue", "Green", "White", "Gray", "Black", "Orange", "Red", "Lemon", "Brown" };

	foreach (const QString &str, names) {
		query.addBindValue(str);
		if(!query.exec())
			qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
	}
}

// ===== PRIVATE SLOTS =====
void MainWindow::OnInput()
{
	if(mInputText->text().length() > 0)
		mButtonSearch->setEnabled(true);
	else
		mButtonSearch->setEnabled(false);
}

void MainWindow::OnSearchClicked()
{
	QSqlQuery query;
	query.prepare("SELECT name FROM colors LIMIT 1 OFFSET ?-1");
	query.addBindValue(mInputText->text().toInt());

	if(!query.exec())
		qWarning() << "MainWindow::OnSearchClicked - ERROR: " << query.lastError().text();

	if(query.first())
		mOutputText->setText(query.value(0).toString());
	else
		mOutputText->setText("not found");
}
